<?php
return [
    'controllerPath' => [
        'app/api/controller/v1' => 'app\\api\\controller\\v1'
    ],
    'swagger' => [
        'schemes' => 'http',
        'host' => 'host',
        'title' => '文档标题',
        'description' => '文档描述，此文档为 swagger 格式的 json, 请使用Swagger UI 渲染成 web。',
    ],
    'response' => [
        'bodyKey' => [
            'code' => 0,
            'msg' => '',
            'data' => null
        ]
    ],
];