<?php
/**
 * docboot插件文件
 */
namespace support\docboot;


class ResponseRenderer extends \DocBoot\Controller\ResponseRenderer
{
    /**
     * @param array $output
     * @return \support\Response
     */
    public function render(array $output):\support\Response
    {

        if (!isset($output['headers']['Content-Type'])) {
            $output['headers']['Content-Type'] = 'application/json;charset=utf-8';
        }
        $bodyKey = array_keys(config('app.response.bodyKey', config('plugin.yesgooo.docboot.conf.response.bodyKey', [])));
        $responseBody = [
            $bodyKey[0] ?? 'code' => 0,
            $bodyKey[1] ?? 'msg' => '',
            $bodyKey[2] ?? 'data' => $output['content'],
        ];
        return response(json_encode($responseBody, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), 200, $output['headers']);
    }
}