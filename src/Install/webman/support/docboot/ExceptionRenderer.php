<?php

namespace support\docboot;

use Exception;

class ExceptionRenderer
{
    /**
     * @param Exception $e
     * @throws Exception
     */
    public function render(Exception $e)
    {
        throw $e;
    }
}