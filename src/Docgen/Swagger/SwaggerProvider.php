<?php
namespace DocBoot\Docgen\Swagger;

use DocBoot\Application;
use Symfony\Component\HttpFoundation\Response;

class SwaggerProvider
{
    /**
     * How to use
     *
     * SwaggerProvider::register($demo, function(Swagger $swagger){
     *          $swagger->host = 'api.example.com',
     *          $swagger->info->description = '...';
     *          ...
     *      },
     *      '/docs')
     *
     * @param Application $app
     * @param string $prefix
     * @param callable $callback
     * @return void
     */
    static public function register(Application $app,
                                    callable $callback = null,
                                    $prefix='/docs')
    {
        $app->addRoute('GET', $prefix.'/swagger.json', function ()use($app, $callback){
            $swagger = new Swagger();
            $swagger->appendControllers($app, $app->getControllers());
            if($callback){
                $callback($swagger);
            }
            return new Response($swagger->toJson());
        });
    }
}