<?php
namespace DocBoot\Entity\Annotations;

use DocBoot\Annotation\AnnotationBlock;
use DocBoot\Annotation\AnnotationTag;
use DocBoot\Entity\EntityContainer;
use DocBoot\Metas\PropertyMeta;

class PropertyAnnotationHandler
{
    /**
     * @param EntityContainer $container
     * @param AnnotationBlock|AnnotationTag $ann
     * @return void
     */
    public function __invoke(EntityContainer $container, $ann)
    {
        $meta = $container->getProperty($ann->name);
        if(!$meta){
            $meta = new PropertyMeta($ann->name);
            $container->setProperty($ann->name, $meta);
        }
        $meta->description = $ann->description;
        $meta->summary = $ann->summary;
    }
}