<?php

namespace DocBoot\Entity\Annotations;

use DocBoot\Annotation\AnnotationBlock;
use DocBoot\Annotation\AnnotationTag;
use DocBoot\Entity\ContainerFactory;
use DocBoot\Entity\EntityContainer;
use DocBoot\Entity\EntityContainerBuilder;
use DocBoot\Entity\MixedTypeContainer;
use DocBoot\Exceptions\AnnotationSyntaxException;
use DocBoot\Utils\AnnotationParams;
use DocBoot\Utils\TypeHint;

class VarAnnotationHandler
{
    /**
     * @param EntityContainer $container
     * @param AnnotationBlock|AnnotationTag $ann
     * @param EntityContainerBuilder $builder
     * @return void
     */
    public function __invoke(EntityContainer $container, $ann, EntityContainerBuilder $builder)
    {
        $params = new AnnotationParams($ann->description, 3);
        if($params->count()){
            $type = $params->getParam(0);
            //TODO 校验type类型
            $target = $ann->parent->name;
            $property = $container->getProperty($target);
            $property or \DocBoot\abort($container->getClassName()." property $target not exist ");
            if($type == null || $type == 'mixed'){
                $property->container = new MixedTypeContainer();
            } else{
                // TODO 判断$type是否匹配
                $property->type = TypeHint::normalize($type, $container->getClassName());
                // TODO 防止递归死循环
                $property->container = ContainerFactory::create($builder, $property->type);
            }
        }else{
            \DocBoot\abort(new AnnotationSyntaxException(
                "The annotation \"@{$ann->name} {$ann->description}\" of {$container->getClassName()}::{$ann->parent->name} require 1 param, 0 given"
            ));
        }

    }
}