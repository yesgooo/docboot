<?php

namespace DocBoot\Entity;

use DI\FactoryInterface;
use DocBoot\Controller\ControllerContainer;
use DocBoot\ORM\ModelContainer;
use Invoker\InvokerInterface as DIInvokerInterface;
use Doctrine\Common\Cache\Cache;
use DocBoot\Entity\Annotations\ClassAnnotationHandler;
use DocBoot\Entity\Annotations\PropertyAnnotationHandler;
use DocBoot\Entity\Annotations\ValidateAnnotationHandler;
use DocBoot\Entity\Annotations\VarAnnotationHandler;
use DocBoot\Annotation\ContainerBuilder;

class EntityContainerBuilder extends ContainerBuilder
{
    static array $DEFAULT_ANNOTATIONS=[
        [ClassAnnotationHandler::class, 'class'],
        [PropertyAnnotationHandler::class, 'properties'],
        [VarAnnotationHandler::class, "properties.*.children[?name=='var'][]"],
        [ValidateAnnotationHandler::class, "properties.*.children[?name=='v'][]"],
    ];

    /**
     * ControllerContainerBuilder constructor.
     * @param FactoryInterface $factory
     * @param DIInvokerInterface $diInvoker
     *
     * @param array $annotations
     */
    public function __construct(FactoryInterface $factory,
                                DIInvokerInterface $diInvoker,
                                array $annotations = []
                                )
    {
        if($annotations){
            parent::__construct($annotations);
        }else{
            parent::__construct(self::$DEFAULT_ANNOTATIONS);
        }
        $this->factory = $factory;
        $this->diInvoker = $diInvoker;
    }

    /**
     * load from class with local cache
     * @param string $className
     * @return EntityContainer
     */
    public function build(string $className): EntityContainer
    {
        return parent::build($className);
    }

    /**
     * @param $className
     * @return EntityContainer
     */
    public function buildWithoutCache($className): EntityContainer
    {
        return parent::buildWithoutCache($className);
    }

    /**
     * @param string $className
     * @return EntityContainer
     */
    protected function createContainer($className): EntityContainer
    {
        return $this->factory->make(EntityContainer::class, ['className'=>$className]);
    }

    protected function handleAnnotation($handlerName, $container, $ann)
    {
        $handler = $this->factory->make($handlerName);
        return $this->diInvoker->call($handler, [$container, $ann]);
    }


    /**
     * @var FactoryInterface
     */
    protected $factory;
    /**
     * @var DIInvokerInterface
     */
    protected $diInvoker;
}