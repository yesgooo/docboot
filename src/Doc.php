<?php

namespace DocBoot;

use DocBoot\Docgen\Swagger\Swagger;

class Doc
{
    public static function getSwaggerJson($fromPath = 'App/Controllers/V1', $namespace = 'App\\Controllers\\V1', callable $callable = null): string
    {
        $app = Application::createByDefault();

        $app->loadRoutesFromPath($fromPath, $namespace);

        $swagger = new Swagger();
        $swagger->appendControllers($app, $app->getControllers());
        if($callable){
            $callable($swagger);
        }

        return $swagger->toJson();
    }
}