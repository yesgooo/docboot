<?php

namespace DocBoot\Controller;

use DI\FactoryInterface;
use Invoker\InvokerInterface as DIInvokerInterface;
use DocBoot\Controller\Annotations\BindAnnotationHandler;
use DocBoot\Controller\Annotations\ClassAnnotationHandler;
use DocBoot\Controller\Annotations\HookAnnotationHandler;
use DocBoot\Controller\Annotations\ParamAnnotationHandler;
use DocBoot\Controller\Annotations\PathAnnotationHandler;
use DocBoot\Controller\Annotations\ReturnAnnotationHandler;
use DocBoot\Controller\Annotations\RouteAnnotationHandler;
use DocBoot\Controller\Annotations\ThrowsAnnotationHandler;
use DocBoot\Controller\Annotations\ValidateAnnotationHandler;
use DocBoot\Annotation\ContainerBuilder;

class ControllerContainerBuilder extends ContainerBuilder
{
    static array $DEFAULT_ANNOTATIONS=[
        [ClassAnnotationHandler::class, 'class'],
        [PathAnnotationHandler::class, "class.children[?name=='path']"],
        [RouteAnnotationHandler::class, "methods.*.children[?name=='route'][]"],
        [ParamAnnotationHandler::class, "methods.*.children[?name=='param'][]"],
        [ReturnAnnotationHandler::class, "methods.*.children[?name=='return'][]"],
        [BindAnnotationHandler::class, "methods.*.children[].children[?name=='bind'][]"],
        [ThrowsAnnotationHandler::class, "methods.*.children[?name=='throws'][]"],
        [ValidateAnnotationHandler::class, "methods.*.children[].children[?name=='v'][]"],
        [HookAnnotationHandler::class, "methods.*.children[?name=='hook'][]"],
    ];

    /**
     * ControllerContainerBuilder constructor.
     * @param FactoryInterface $factory
     * @param DIInvokerInterface $diInvoker
     * @param array $annotations
     */
    public function __construct(FactoryInterface $factory,
                                DIInvokerInterface $diInvoker,
                                array $annotations = [])
    {
        if($annotations){
            parent::__construct($annotations);
        }else{
            parent::__construct(self::$DEFAULT_ANNOTATIONS);
        }

        $this->factory = $factory;
        $this->diInvoker = $diInvoker;
    }

    /**
     * load from class with local cache
     * @param string $className
     * @return ControllerContainer
     */
    public function build(string $className): ControllerContainer
    {
        return parent::build($className);
    }

    /**
     * @param $className
     * @return ControllerContainer
     */
    public function buildWithoutCache($className): ControllerContainer
    {
        return parent::buildWithoutCache($className);
    }

    /**
     * @param string $className
     * @return ControllerContainer
     */
    protected function createContainer($className): ControllerContainer
    {
        return $this->factory->make(ControllerContainer::class, ['className'=>$className]);
    }

    protected function handleAnnotation($handlerName, $container, $ann)
    {
        $handler = $this->factory->make($handlerName);
        return $this->diInvoker->call($handler, [$container, $ann]);
    }


    /**
     * @var FactoryInterface
     */
    private FactoryInterface $factory;
    /**
     * @var DIInvokerInterface
     */
    private DIInvokerInterface $diInvoker;
}