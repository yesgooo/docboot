<?php

namespace DocBoot\Controller\Annotations;


use DocBoot\Annotation\AnnotationBlock;
use DocBoot\Annotation\AnnotationTag;
use DocBoot\Controller\ControllerContainer;

class ClassAnnotationHandler
{
    /**
     * @param ControllerContainer $container
     * @param AnnotationBlock|AnnotationTag $ann
     */
    public function __invoke(ControllerContainer $container, $ann)
    {
        $ref = new \ReflectionClass($container->getClassName());

        $container->setDescription($ann->description);
        $container->setSummary($ann->summary);
        $container->setFileName($ref->getFileName());
    }
}