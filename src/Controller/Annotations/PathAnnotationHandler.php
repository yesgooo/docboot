<?php

namespace DocBoot\Controller\Annotations;

use DocBoot\Annotation\AnnotationBlock;
use DocBoot\Annotation\AnnotationTag;
use DocBoot\Controller\ControllerContainer;
use DocBoot\Utils\AnnotationParams;

class PathAnnotationHandler
{

    /**
     * @param ControllerContainer $container
     * @param AnnotationBlock|AnnotationTag $ann
     */
    public function __invoke(ControllerContainer $container, $ann)
    {
        $params = new AnnotationParams($ann->description, 2);
        $container->setUriPrefix($params->getParam(0, ''));
    }
}