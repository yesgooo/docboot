<?php
ini_set('date.timezone','Asia/Shanghai');

require __DIR__ . '/../vendor/autoload.php';

$json = \DocBoot\Doc::getSwaggerJson('demo/app/controllers', 'demo\\app\\controllers', function ($swagger){
    $swagger->schemes = ['http'];
    $swagger->host = 'your host';
    $swagger->info->title = '文档标题';
    $swagger->info->description = "此文档为 swagger 格式的 json, 请使用Swagger UI 渲染成 web。";
    $swagger->externalDocs = new \DocBoot\Docgen\Swagger\Schemas\ExternalDocumentationObject();
    $swagger->externalDocs->description = '';
    $swagger->externalDocs->url = '';
});

echo $json;