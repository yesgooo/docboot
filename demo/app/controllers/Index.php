<?php
namespace demo\app\controllers;

use demo\app\entities\UserPO;

/**
 * controllers title for Index
 *
 * controllers description for Index
 * and more description
 * and more
 * @path index/v1
 */
class Index
{
    /**
     * method title for indexAction
     *
     * method description for indexAction
     * and more description
     * and more
     * @route POST /
     * @param UserPO[] $user 用户对象
     * @param int $id 用户ID
     * @param string $name 姓名
     * @return string
     */
    public function indexAction($user, $id, $name)
    {
        return "{$id}, {$name}" . $user->name;
    }
}