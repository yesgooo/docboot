<?php
namespace demo\app\entities;

class UserPO
{
    /**
     * ID
     * @var int
     */
    public int $id;

    /**
     * 姓名
     * @var string
     */
    public string $name;
    /**
     * 汽车
     * @var CarPo[]
     */
    public array $cars;
}