<?php

namespace demo\app\entities;

class CarPo
{
    /**
     * 品牌
     * @var string
     */
    public string $brand;

    /**
     * 型号
     * @var string
     */
    public string $model;
}